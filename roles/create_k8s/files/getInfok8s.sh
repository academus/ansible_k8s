#!/bin/bash

#autenticando GCP
gcloud auth activate-service-account --key-file=./quero-proj-c096260a2f56.json --project="quero-proj"
gcloud container clusters get-credentials quero-cluster --zone us-central1-a --project "quero-proj"

URL=$(kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}');
CA=$(kubectl get secret $(kubectl get secrets | tail -1 | awk '{ print $1 }') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode);

kubectl apply -f .roles/create_k8s/files/gitlab-admin-service-account.yaml

token=$(kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}') | grep token: | awk  '{ print $2 }');

printf "$URL\n\n$CA\n\n$token" >  ./roles/create_k8s/files/gitlab_k8s.conf

exit 0
